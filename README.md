# Disponibilizando aplicações utilizando AWS Elastic Beanstalk + Docker + Terraform e Gitlab-CI

## Sobre :star:

Utilizando Terraform e Gitlab-CI foi construido uma infraestrutura na AWS, para disponibilizar uma API Rest (através do Elastic Beanstalk) feita em Node.js e rodando em Docker.

> O AWS Elastic Beanstalk é um serviço de fácil utilização para implantação e escalabilidade de aplicações e serviços da web desenvolvidos com Java, .NET, PHP, Node.js, Python Ruby, Go e Docker em servidores familiares como Apache, Nginx, Passenger e IIS.

O Elastic Beanstalk foi escolhido por ser um facilitador na construção de ambientes.

## Recursos Utilizados :page_facing_up:

* Terraform [v0.14.4](https://releases.hashicorp.com/terraform/0.14.4/)
* [AWS Elastic Beanstalk](https://aws.amazon.com/pt/elasticbeanstalk/)
* [AWS Aurora - MySQL](https://aws.amazon.com/pt/rds/aurora/mysql-features/)
* Docker CE [v19.03](https://docs.docker.com/engine/install/)
* Node.js [14.15.4](https://nodejs.org/en/)
* [Gitlab-CI/CD](https://docs.gitlab.com/ee/ci/README.html)
* [Sonarcloud](https://sonarcloud.io/)
* [Snyk](https://snyk.io/product/open-source-security-management/)
* [Slack](https://docs.gitlab.com/ee/user/project/integrations/slack.html)

## Ambiente que será construído :eyes:

Uma infraestrutura de rede e banco de dados será criada para atender a aplicação. As instancias e banco estarão em uma rede privada e somente o Load Balancer estará na rede pública.

O ambiente estará na região de N. Virginia (us-east-1) e utilizará as zonas (us-east-1a, us-east-1b e us-east-1c).

Será feito o build da aplicação para uma imagem em docker e disponibilizada no Docker Hub. Uma analise de código e vulnerabilidade é executado antes do build.

Os processos de criação estão sendo feitos pelo Gitlab-CI, sendo que as ações do Terraform requerem aprovação manual para construção.

Foi criado uma integração com o Slack para notificação.

## Requisitos e configurações iniciais :mag_right:

* Será necessário ter um usuário com acesso programático na AWS. Este usuário precisará ter permissões para criar/remover recursos como EC2, ALB, S3, CloudWatch e outros, além de precisar criar Roles/Policy no IAM. Para validação/demonstração, um usuário com a Policy de AdministratorAccess é mais do que suficiente. Em ambientes reais siga a politica de menor privilégio, ok?

## Subindo o ambiente :rocket:

1- Criar a infraestrutura de rede (branch infra).
2- Criar o banco de dados (branch banco).
3- Criar a variavel HOST, no Gitlab, com o valor a URL de acesso ao banco.
4- Realizar o build da imagem Docker (branch api).
5- Criar a aplicação (branch eb).

## Acessando a API :computer:

Abra o Postman (ou outro recurso) para realizar as ações:

| Método | Verbo HTTP | Endpoint |
|---|---|---|
| Criando um novo Produto | POST | `http://myapi-products-desafio.us-east-1.elasticbeanstalk.com/api/products/` |
| Listando todos os Produtos | GET | `http://myapi-products-desafio.us-east-1.elasticbeanstalk.com/api/products/` |
| Buscando Produto pelo product_id | GET | `http://myapi-products-desafio.us-east-1.elasticbeanstalk.com/api/products/{id}`|
| Atualizando Produto pelo product_id | PUT | `http://myapi-products-desafio.us-east-1.elasticbeanstalk.com/api/products/{id}`|
| Delanto Produto pelo product_id | DELETE | `http://myapi-products-desafio.us-east-1.elasticbeanstalk.com/api/products/{id}`|

Exemplo de entrada e campos obrigatórios:

```sh
{
    "title": "calça jeans",
    "sku": "SCK-4896",
    "barcode": "8795789",
    "description": "calça jeans desbotada",
    "attributes": "tamanho 38",
    "price": "120.90"
}
```

## Observação e informações relevantes :exclamation:

Alguns pontos merecem atenção e que não foram tratados por se tratar de uma demostração:

* Utilização de HTTPS/TLS.
* VPC Endpoint para comunicação entre as instâncias e o Elastic Beanstalk.
* Utilizar o AWS WAF no Load Balancer.
* Versionamento e criptografia no bucket que armazena o Dockerrun.
* AMI customizada/hardening e com criptografia de disco.
* Utilizar um Registry privado.